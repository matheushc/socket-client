import socket
from sys import argv

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.connect(('127.0.0.1', 8080))
    f = open(argv[1], "rb")
    l = f.read(1024)
    while (l):
        s.send(l)
        l = f.read(1024)
    print(l)
